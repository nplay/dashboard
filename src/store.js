import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from 'vuex-persistedstate'
import UserModule from './store/modules/userModule'
import CategorysTreesModule from './store/modules/categoryTreesModule'
import DialogsModules from './store/modules/dialogsModule'
import OperatorModule from './store/modules/operatorModule'
import CurrentPageModule from './store/modules/currentPageModule'
import PeriodModule from './store/modules/periodModule'
import ThemeModule from './store/modules/themeModule'
import CurrentPopupModule from './store/modules/currentPopupModule'
import ThemeLineFilterModule from './store/modules/themeLineFilterModule'

Vue.use(Vuex);

const workflowsDefault = {
  menuOrganizarListPage: {
    itens: new Array
  },
  dashboardPage: {
    dateFrom: null,
    dateTo: null,
    salesMiniChart: { collection: [{Periodo: "2018-12-28", Itens: 0, Total: 0}] },
    visualizedItemsMiniChart: { collection: [{Periodo: "2018-12-28", Itens: 0, Total: 0}] },
    prospectItemsMiniChart: { collection: [{Periodo: "2018-12-28", Itens: 0, Total: 0}] },
    sessionsMiniChart: { collection: [{Periodo: "2018-12-28", Itens: 0, Total: 0}] }
  }
}

export default new Vuex.Store({
  plugins: [
    createPersistedState({
    getState: (key) => {
      let item = sessionStorage.getItem(key)
      return item != null ? JSON.parse(item) : null
    },
    setState: (key, state) => {
      try {
        sessionStorage.setItem(key, JSON.stringify(state))
      } catch (error) {
        console.log(state)
        debugger
      }
    }
  })],
  state: {
    env: {
      API_HOST: 'https://api.nplay.com.br',
      API_SERVICE_HOST: 'https://api.service.nplay.com.br/v1',
      AUTHORIZATION_TOKEN: null
    },
    workflows: {
    },
    userProfile: {}
  },
  mutations: {
    userProfile(state, user){
      state.userProfile = user
    },
    resetWorkflow(state, key){
      let defaultData = !workflowsDefault[key] ? null : Object.assign({}, workflowsDefault[key])
      Vue.set(state.workflows, key, defaultData);
    },
    workflows(state, {property, value}){
      Vue.set(state.workflows, property, value);
    }
  },
  actions: {
  },
  getters: {
    workflows: (state => {
      return state.workflows
    }),
    workflow: (state) => ({workflowID}) => {
      return state.workflows[workflowID]
    }
  },

  modules: {
    userProfile: UserModule,
    categoryTrees: CategorysTreesModule,
    dialogs: DialogsModules,
    operator: OperatorModule,
    currentPage: CurrentPageModule,
    period: PeriodModule,
    theme: ThemeModule,
    popup: CurrentPopupModule,
    themeLineFilter: ThemeLineFilterModule
  }
});
