
class MountHelper
{
    tree(categorieCollection)
    {
        let arrayCat = categorieCollection.map(cat => {
            return cat.toObject();
        })

		let getPaths = function(node, map, list, path)
		{
			if(path == undefined)
				path = new Array(node.name);

			let parentNode = list[map[node.parent_id]]

			if(parentNode == undefined)
				return path.reverse().join(' > ')

            let parentIsRoot = 1
            if(parentNode.parent_id != parentIsRoot)
			    path.push(parentNode.name)

			return getPaths(parentNode, map, list, path)
		}

        let fnListToTree = (list) => {
            var map = {}, node, roots = [], i;
    
            for (i = 0; i < list.length; i += 1)
            {
                map[list[i].id] = i; // initialize the map
                list[i].children = []; // initialize the children
                list[i].path = null;
            }

            for (i = 0; i < list.length; i += 1)
            {
                node = list[i];
    
                if (node.parent_id == 1)
                    roots.push(node);
                else
                {
                    node.path = getPaths(node, map, list)
                    list[map[node.parent_id]].children.push(node);
                }
            }
    
            return roots;
        }
        
        return fnListToTree(arrayCat)
    }  
}

export default MountHelper