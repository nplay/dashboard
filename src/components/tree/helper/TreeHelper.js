
import MountHelper from "@/components/tree/helper/Mount.js"

class CategoryTreeHelper
{
    constructor(tree)
    {
        this._tree = tree == undefined ? null : tree;
        this._mountHelper = new MountHelper();
    }

    async _mount()
    {
       this._tree = await this._mountHelper.tree()
       return this
    }

    getMaxLevels()
    {
        let level = 0
        let result = this.getLevel()

        while(result.currentLevel.length > 0)
        {
            ++level
            result = this.getLevel(result.nextLevel)
        }

        return level
    }

    searchByAttribute(attribute, value)
    {
        let iterator = this.iterate();
        let cursor = iterator.next();

        while(cursor.done == false)
        {
            let node = cursor.value

            if(node.hasOwnProperty(attribute) && node[attribute] == value)
                return node

            cursor = iterator.next();
        }

        return false;
    }

    getLevelBy(level)
    {
        let nodes = null
        let nextNode = null
        let currentNode = null

        for(let i = 1; i <= level; ++i)
        {
            nodes = this.getLevel(nextNode)
            nextNode = nodes.nextLevel
            currentNode = nodes.currentLevel

            if(currentNode.length == 0)
            {
                console.warn(`Level ${level} is empty`)
                return new Array();
            }
        }

        return nodes.currentLevel
    }

    getLevel(nodes)
    {
        if(nodes == undefined)
        {
            nodes = new Array()

            let iterator = this.iterate(null, false)
            let cursor = iterator.next()

            while(cursor.done == false)
            {
                nodes.push(cursor.value)
                cursor = iterator.next()
            }
        }

        let currentNodes = new Array()
        let nextNodes = new Array()

        for(let node of nodes)
        {
            currentNodes.push(node)
            
            for(let children of node.children)
                nextNodes.push(children)
        }

        return {
            currentLevel: currentNodes,
            nextLevel: nextNodes
        }
    }

    *iterate(currentNode, iterateChild)
    {
        if(currentNode == undefined)
        {
            for(let root of this._tree)
                yield* this.iterate(root, iterateChild)
        }
        else
        {
            yield currentNode
            
            if(iterateChild == false)
                return;
                
            for(let child of currentNode.children)
                yield*  this.iterate(child, iterateChild)
        }
    }

    /**
     * Returns node relative into path passed
     * @param {String} path
     */
    *iteratePath(path)
    {
        let node = this.search(path)

        if(typeof node == undefined)
            throw new Error(`Fail iteratePath ${path}, node not found`)

        //Transpass node
        yield node

        let partsPath = path.split(' > ')
        partsPath.pop()
        path = partsPath.join() 

        if(Array.isArray(path) == false) //Final path
        {
            node = this.search(path)
            return yield node
        }
        else
            yield* this.iteratePath(path)
    }

    search(path)
    {
        let iterator = this.iterate();
        let cursor = iterator.next();

        while(cursor.done == false)
        {
            let node = cursor.value

            if(node.path == path)
                return node

            cursor = iterator.next();
        }

        return false;
    }

    /**
     * Trace parent nodes into node searched
     */
    traceById(id)
    {
        let iterator = this.iterate();
        let cursor = iterator.next();
        
        return this._traceByAttribute(iterator, cursor, 'id', id)
    }

    _traceByAttribute(iterator, cursor, attribute, value)
    {
        let tracer = new Array()

        while(cursor.done == false)
        {
            let node = cursor.value

            if(node.children.length > 0 && node[attribute] != value)
                tracer.push(node)

            if(node[attribute] == value)
                return tracer;

            cursor = iterator.next();
        }

        return null
    }

    /**
     * Get node searched by ID
     */
    getById(id)
    {
        let iterator = this.iterate();
        let cursor = iterator.next();
        
        return this._getByAttribute(iterator, cursor, 'id', id)
    }

    _getByAttribute(iterator, cursor, attribute, value)
    {
        while(cursor.done == false)
        {
            let node = cursor.value

            if(node[attribute] == value)
                return node;

            cursor = iterator.next();
        }

        return null
    }
    
    assignAttributes(attributes)
    {
        let iterator = this.iterate();
        let cursor = iterator.next();

        while(cursor.done == false)
        {
            let node = cursor.value

            if(node != undefined)
                Object.assign(node, attributes)

            cursor = iterator.next();
        }
    }

    sortByAttribute(attributeName)
    {
        let iterator = this.iterate();
        let cursor = iterator.next();

        while(cursor.done == false)
        {
            let node = cursor.value

            this.sort(node.children, attributeName)
            cursor = iterator.next();
        }
    }

    sort(node, attributeName)
    {
        node.sort((a, b) => {
            if(!a.hasOwnProperty(attributeName) || !b.hasOwnProperty(attributeName))
                throw new Error(`Property: ${attributeName} not exists in object sort`)

            let result = a[attributeName] - b[attributeName]

            if(result < 0)
                return 1;
            else if(result > 0)
                return -1;
            else
                return 0;
        })
    }
}

export default CategoryTreeHelper