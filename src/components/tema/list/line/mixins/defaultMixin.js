
const DefaultMixin = {
   computed: {
      line: {
         get: function(){
            let line = this.$store.getters['theme/line/byID']({
               workflowID: this.workflowID,
               lineID: this.lineID
            })

            return line
         },

         set: function(val){

            let line = this.$store.getters['theme/line/byID']({
               workflowID: this.workflowID,
               lineID: this.lineID
            })
            
            line = Object.assign(JSON.parse(JSON.stringify(line)), val)

            this.$store.dispatch('theme/line/set', {
               workflowID: this.workflowID,
               payload: line
            })
         }
      }
   },

   methods: {
      async _close()
      {
         let line = this.line
         line.additionalData.currentOption = null;
      },
   },

   props: {
      lineID: {
        type: String,
        required: true
      }
   },
}

export default DefaultMixin