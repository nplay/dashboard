
const HTMLMixin = {
    components: {
    },

    methods: {
        getTheme() {
          let html = this.theme.designModel.html
          return this.decodeHTML(this.decodeBase64(html))
        },
  
        decodeBase64(data)
        {
          return window.atob(data)
        },
  
        encodeBase64(data)
        {
          return window.btoa(data)
        },
  
        decodeHTML(html) {
          return decodeURIComponent(html)
        },
  
        encodeHTML(html) {
          return encodeURIComponent(html)
        }
    },
}

export default HTMLMixin