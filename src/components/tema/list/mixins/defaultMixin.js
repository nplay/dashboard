
import IndexAPI from "@/api/themes/site/indexApi.js"
import SnackHelper from "@/helper/snack.js";

const DefaultMixin = {
    components: {
    },

    props: {
      workflowID:{
         type: String,
         required: true
      }
    },

    data(){
      return {
         themeSiteAPI: new IndexAPI(),
         snackHelper: new SnackHelper()
      }
    },
    
    watch: {
    },

    computed: {

      theme:{
         get: function(){
            return this.$store.getters['theme/get']({workflowID: this.workflowID})
         },
         set:function(val){
            this.saveWorkflow(val)
         }
      }

   },

   methods: {
      async _close(update = true)
      {
         let theme = JSON.parse(JSON.stringify(this.theme))
         theme.additionalData.currentOption = null;

         if(update)
         {
            let response = await this.update(theme)
            this.theme = Object.assign(this.theme, response.data.data)
         }
      },

      async _refresh()
      {
         try {
            let updTheme = await this.get()
            updTheme.additionalData.currentOption = this.theme.additionalData.currentOption
            
            this.theme = Object.assign({}, this.theme, updTheme)
            return this.theme
         } catch (error) {
            this.errorMessage()
            return null
         }
      },

      async get()
      {
         let response = await this.themeSiteAPI.all({
            filters: {
               
               _id: this.theme._id
            }
         });

         return response.data.data[0]
      },

      toggleLoading()
      {
         let data = this.$store.getters['currentPage/get']({workflowID: 'default'})

         this.$store.dispatch('currentPage/setLoading', {
            workflowID: 'default',
            payload: !data.loading
        })
      },

      successMessage()
      {
         this.snackHelper.setSuccess('Tudo certo!').display()
      },

      errorMessage()
      {
         this.snackHelper.setError('Falha durante o processamento').display()
      },

      async update(theme)
      {
         this.toggleLoading();

         theme = theme == undefined ? this.theme : theme
         let objectID = theme._id

         try {
            let response = await this.themeSiteAPI.patch(theme, objectID);
            this.successMessage()
            this.toggleLoading();

            return response
         } catch (error) {
            this.errorMessage()
            this.toggleLoading();
            return null
         }
      },

      async insert(theme)
      {
         this.toggleLoading();

         try {
            let response = await this.themeSiteAPI.put(theme);
            this.successMessage()
            this.toggleLoading();

            return response
            
         } catch (error) {
            this.errorMessage()
            this.toggleLoading();

            return null;
         }
      },

      async delete(theme)
      {
         this.toggleLoading();

         try {
            let response = await this.themeSiteAPI.delete(theme._id);
            this.successMessage()
            this.toggleLoading();

            return response
            
         } catch (error) {
            this.errorMessage()
            this.toggleLoading();

            return null;
         }
      },

      saveWorkflow(theme)
      {
         this.$store.dispatch('theme/set', {
            workflowID: this.workflowID,
            payload: theme
         })
      }
   }
}

export default DefaultMixin