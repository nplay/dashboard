
import WorkflowStoreHelper from "@/helper/workflowStoreHelper.js"
import PopupIndexAPI from "@/api/themes/popup/indexApi.js"

const DataTableMixin = {
   props: {
   },

   methods: {
      async loadThemes(type){
         let API = new PopupIndexAPI()

         let body = {
         "filters": {
               "page": [type]
            }
         }

         let themes = []

         try {
            let response = await API.all(body)
            themes = response.data.data
         } catch (error) {
            
         }

         let helper = new WorkflowStoreHelper("listaTemaPopup")
         let data = helper.get()

         var me = this;
         data.collection = themes.map(theme => {
            me.$set(theme, 'additionalData', theme.additionalData)
            theme.additionalData.currentOption = null
            
            return theme
         })

         helper.save(data)
      }
   },
   
   computed: {
      data() {
         return new WorkflowStoreHelper("listaTemaPopup", {
            collection: new Array,
            additionalData: {
              currentOption: null
            },
            headers: [
               {
                  text: "Tema",
                  align: "center",
                  sortable: false,
                  value: "name"
               },
               {
                  text: "Tipo",
                  align: "center",
                  sortable: false,
                  value: "typeName"
               },
               {
                  text: "Ação",
                  align: "center",
                  sortable: false,
                  value: "actionName"
               },
               {
                  text: "Opções",
                  align: "center",
                  sortable: false,
                  value: "options"
               }
            ],
            type: null
         }).get()
      },

      collection() {
         return this.data.collection
      }
   },

   data() {
      return {
      }
   }
}

export default DataTableMixin