
import PopupIndexAPI from "@/api/themes/popup/indexApi.js"
import SnackHelper from "@/helper/snack.js";
import CurrentPopupMixin from "@/mixin/store/currentPopupMixin.js";

const DefaultMixin = {
   mixins: [CurrentPopupMixin],

   props: {
      workflowID: {
         type: String,
         required: true
      }
   },

   computed: {
      //Alias to 'currentPopup'
      theme: {
         get: function(){
            return this.currentPopup
         },
         set: function(val){
            this.currentPopup = val
         }
      }
   },

   data() {
      return {
         snackHelper: new SnackHelper(),
         popupAPI: new PopupIndexAPI()
      }
   },

   methods: {
      async setCurrentOption(option, theme) {

         if(this.theme._id && this.theme._id != theme._id)
            await this._clearCurrentOption()

         theme.additionalData.currentOption = option
         this.saveWorkflow(theme)
      },

      async _clearCurrentOption()
      {
         this.theme.additionalData.currentOption = null;
      },
      
      async _close(canUpdate = true, clearCurOption = true)
      {
         if(clearCurOption)
            await this._clearCurrentOption()
         
         try {
            this.toggleLoading()

            if(canUpdate)
            {
               this.saveWorkflow(this.theme)
               await this.update()
               this.successMessage()
            }
            
         } catch (error) {
            this.errorMessage()
         }
         finally{
            this.toggleLoading()
         }
      },

      toggleLoading()
      {
         let data = this.$store.getters['currentPage/get']({workflowID: 'default'})

         this.$store.dispatch('currentPage/setLoading', {
            workflowID: 'default',
            payload: !data.loading
        })
      },

      async _refresh()
      {
         try {
            let theme = await this.get()
            this.saveWorkflow(theme)
            return theme
            
         } catch (error) {
            this.errorMessage()
         }
      },

      async get(){
         let response = await this.popupAPI.all({
            filters: {
               _id: this.theme._id
            }
         });

         return response.data.data[0] || null
      },

      async update(){
         return await this.popupAPI.patch(this.theme, this.theme._id);
      },

      async delete(id){
         return await this.popupAPI.delete(id)
      },

      async insert(data){
         return await this.popupAPI.put(data)
      },

      saveWorkflow(theme)
      {
         this.theme = theme
      },

      successMessage()
      {
         this.snackHelper.setSuccess('Tudo certo!').display()
      },

      errorMessage()
      {
         this.snackHelper.setError('Falha durante o processamento').display()
      }
   }
}

export default DefaultMixin