
import PeriodMixin from '@/mixin/store/periodMixin.js'

const Mixin = {
  mixins: [
    PeriodMixin
  ],

  props: {
    workflowID: {
      type: String,
      required: true
    }
  },

  components: {
  },

  data() {
    return {
      datSource: []
    }
  },

  computed: {
    dataSource: {
      get: function () {
        return this.datSource
      },
      set: function (val) {
        this.datSource = val
      }
    }
  },

  watch: {
    'period.fromDays': async function(val, oldVal) {
      await this.updateDataSource()
    }
  },

  methods: {
    async updateDataSource() {
      this.dataSource = []
    }
  }
}

export default Mixin