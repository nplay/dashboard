
import EChart from '@/components/chart/echart';
import Material from 'vuetify/es5/util/colors';
import PeriodMixin from '@/mixin/store/periodMixin.js'

const PieMixin = {
  mixins: [
    PeriodMixin
  ],

  props: {
    workflowID: {
      type: String,
      required: true
    }
  },

  components: {
    EChart
  },

  data() {
    return {
      color: Material,
      datSource: [{ value: 0, name: "Nenhum" }]
    }
  },

  computed: {
    dataSource: {
      get: function () {
        return this.datSource
      },
      set: function (val) {
        let length = this.datSource.length

        val.map(item => this.datSource.push(item))
        this.datSource.splice(0, length)
      }
    }
  },

  watch: {
    'period.fromDays': async function(val, oldVal) {
      await this.updateDataSource()
    }
  },

  methods: {
    async updateDataSource() {
      this.dataSource = [{ value: 0, name:  'Nenhum' }]
    }
  }
}

export default PieMixin