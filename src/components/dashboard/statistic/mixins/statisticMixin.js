
import PeriodMixin from '@/mixin/store/periodMixin.js'

const PieMixin = {
  mixins: [
    PeriodMixin
  ],

  props: {
    workflowID: {
      type: String,
      required: true
    }
  },

  components: {
  },

  data() {
    return {
      datSource: null
    }
  },

  computed: {
    dataSource: {
      get: function () {
        return this.datSource
      },
      set: function (val) {
        this.datSource = val
      }
    }
  },

  watch: {
    'period.fromDays': async function(val, oldVal) {
      await this.updateDataSource()
    }
  },

  methods: {
    async updateDataSource() {
      this.dataSource = null
    }
  }
}

export default PieMixin