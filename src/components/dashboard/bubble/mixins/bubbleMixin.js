
import PeriodMixin from '@/mixin/store/periodMixin.js'

const Mixin = {
  mixins: [
    PeriodMixin
  ],

  props: {
    workflowID: {
      type: String,
      required: true
    }
  },

  components: {
  },

  data() {
    return {
      datSource:       [
        ['ID', 'Data', 'Acessos', 'Nome', 'Acessos (em %)']
      ]
    }
  },

  computed: {
    dataSource: {
      get: function () {
        return this.datSource
      },
      set: function (val) {
        this.datSource = val
      }
    }
  },

  watch: {
    'period.fromDays': async function(val, oldVal) {
      await this.updateDataSource()
    }
  },

  methods: {
    async updateDataSource() {
      this.dataSource =       [
        ['ID', 'Data', 'Acessos', 'Nome', 'Acessos (em %)'],
        ['CAN',  new Date('2020-01-01 10:00'), 2, 'Produto 1', .66],
        ['BOD',  new Date('2020-01-15 12:00'), 1, 'Produto 2', .33],
        ['BOD2',  new Date('2020-01-15 13:00'), 8, 'Produto 3', .07],
        ['CAN',  new Date('2020-01-30 07:00'), 1, 'Produto 1', .66]
      ]
    }
  }
}

export default Mixin