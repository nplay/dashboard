
import UserApi from "@/api/userApi"
import CategoryRequest from "@/api/service/api/category/request"
import StorageSimilarRequest from "@/api/service/storage/SimilarRequest.js"
import ViewToViewAPI from "@/api/service/storage/ViewToViewRequest.js"
import BuyToBuyRequest from "@/api/service/analytics/BuyToBuyRequest.js"

const DataMixin = {
   components: {
   },
   data: () => {
      return {

      }
   },
   watch: {
   },
   methods: {
      async saveUserProfile(step = 2)
      {
         let store = this.$store.getters['userProfile/store/get']
         store.instalationStep = step
         store = Object.assign(store, this.storeModel)

         let userAPI = new UserApi()
         await userAPI.patch(store)
         
         this.$store.dispatch('userProfile/store/set', store)
      },

      async runSchedules()
      {
         try {
            let viewToViewAPI = new ViewToViewAPI();
            await viewToViewAPI.get()

            let buyToBuyRequest = new BuyToBuyRequest()
            await buyToBuyRequest.get()

            let serviceAPI = new CategoryRequest()
            await serviceAPI.get()

            let storageSimilarServiceAPI = new StorageSimilarRequest();
            await storageSimilarServiceAPI.get()
         } catch (error) {
            console.log(error)
         }
       },

      goRedirector()
      {
         //Invoke midleware
         this.$router.push({path: '/dashboard'})
      }
   }
}

export default DataMixin