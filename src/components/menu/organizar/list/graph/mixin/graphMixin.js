
import WorkflowStoreHelper from "@/helper/workflowStoreHelper.js"

const GraphMixin = {
    components: {
    },
    props: {
    },
    filters: {
      toString: function (value) {
        return value + ''
      }
    },
    data: () => {
      return {
        graphMetric: 0,
        summaryType: null,
        categoryListID: 'menuOrganizarListPage'
      }
    },
    computed: {
      categoryList(){
        return new WorkflowStoreHelper(this.categoryListID).get().itens || null
			}
    },
    watch: {
      categoryList(val, oldVal)
      {
        this.processSummary()
      }
    },
    methods: {
      async processSummary(categoryModel)
      {
        let summaryCollection = categoryModel ? await this._getFromMenu(categoryModel) : await this._getFromCategoryList()

        //Every month
        var date = new Date();
        var firstDayTime = new Date(date.getFullYear(), date.getMonth(), 1).getTime();

        let total = parseInt(0)

        summaryCollection.map((summary) => {
          let dateTime = new Date(summary.date).getTime()
          let sumVal = dateTime >= firstDayTime ? summary.total : 0

          total += parseFloat(sumVal)
        })

        this.graphMetric = total
      },

      async _getFromCategoryList(catList)
      {
        if(!catList)
          catList = this.categoryList

        let category = catList.find(category => {
          return category.isCurrent
        })

        return !category ? new Array : await this._getFromCategory(category) || new Array
      },

      async _getFromCategory(category)
      {
        var me = this
        let lastCurrentWriteTime = new Date(category.lastCurrentWrite).getTime()

        let summaryCollection = new Array()

        for(let metric of category.metrics)
        {
          if(metric.type != me.summaryType)
            continue;
          
          for(let summary of metric.summary)
          {
            let objTime = new Date(summary.date).getTime()
            let result = objTime >= lastCurrentWriteTime

            if(result)
              summaryCollection.push(summary)
          }
        }

        return summaryCollection
      },

      async _getFromMenu(categoryModel)
      {
        let metric = await this._getFromCategory(categoryModel)
        
        return metric || new Array
      }
    }
}

export default GraphMixin