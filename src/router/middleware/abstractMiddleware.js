

class AbstractMiddleware
{
   constructor(router, to, from)
   {
      this._to = to
      this._from = from
      this._router = router

      this._pushTo = null
   }

   run()
   {
      throw new Error(`Please implement in child class`)
   }

   pushTo()
   {
      return this._pushTo
   }
}

export default AbstractMiddleware