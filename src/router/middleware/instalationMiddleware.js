
import AbstractMiddleware from './abstractMiddleware'
import SnackHelper from "@/helper/snack.js";

class InstalationMiddleware extends AbstractMiddleware
{
   constructor(router, to, from)
   {
      super(router, to, from);

      this.steps = new Object({
         'bis2bis.1': { path: `/instalation/bis/data-api` },
         'bis2bis.2': { path: '/instalation/bis/waitProducts' }
      })
   }

   run()
   {
      let step = this._instalationStep()
      return this._redirect(step)
   }

   _routeInPaths(path)
   {
      let routeIsPath = Object.values(this.steps).some( step => {
         return step.path == path
      })

      return routeIsPath
   }

   _redirect(instalationStep)
   {
      if(instalationStep == undefined || this._to.meta.public == true)
         return false;
      else if(instalationStep == 0 || this._routeInPaths(this._to.path))
         return false;
      
      if(!this.steps.hasOwnProperty(instalationStep))
         throw new Error(`Instalation step ${instalationStep} not mapped`)
      
      new SnackHelper()
         .setWarning('Atenção, você precisa finalizar a configuração de seu painel primeiro.')
         .display()

      let path = this.steps[instalationStep].path

      if(this._to.path == path)
         return false;

      this._pushTo = { path: path }
      return true
   }

   _instalationStep()
   {
      let store = this._router.app.$store.getters['userProfile/store/get']

      if(store && store.instalationStep)
         return `${store.platform}.${store.instalationStep}`
      
      return null
   }
}

export default InstalationMiddleware