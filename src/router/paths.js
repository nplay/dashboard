export default [
  {
    path: '*',
    meta: {
      public: true,
    },
    redirect: {
      path: '/404'
    }
  },
  {
    path: '/404',
    meta: {
      public: true,
    },
    name: 'NotFound',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/NotFound.vue`
    )
  },
  {
    path: '/403',
    meta: {
      public: true,
    },
    name: 'AccessDenied',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/Deny.vue`
    )
  },
  {
    path: '/500',
    meta: {
      public: true,
    },
    name: 'ServerError',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/Error.vue`
    )
  },
  {
    path: '/newUser',
    meta: {
      public: true,
    },
    name: 'Register',
    component: () => import(
      `@/views/Register.vue`
    )
  },
  {
    path: '/contact',
    meta: {
      public: true,
    },
    name: 'Contact',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/Contact.vue`
    )
  },
  {
    path: '/tema/popup',
    name: 'tema/popup',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/tema/popup/Index.vue`
    ),
    children: [
      {
        path: '/tema/popup/list/:type',
        meta: {
          public: false,
        },
        name: 'tema/popup/list',
        props: true,
        components: {
          default: () => import(
            `@/views/tema/popup/List.vue`
          )
        }
      }
    ]
  },
  {
    path: '/tema',
    name: 'tema/',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/tema/Index.vue`
    ),
    children: [
      {
        path: '/tema/list/type/:typeTheme',
        meta: {
          public: false,
        },
        name: 'tema/list',
        props: true,
        components: {
          default: () => import(
            `@/views/tema/List.vue`
          )
        }
      }
    ]
  },
  {
    path: '/menu/organizar',
    //meta: { breadcrumb: true },
    meta: {  },
    name: 'menu/organizar',
    redirect: {
      name: 'menu/organizar/list'
    },
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/menu/Organizar.vue`
    ),
    children: [
      {
        path: '/menu/organizar/list',
        meta: {
          public: false,
        },
        name: 'menu/organizar/list',
        props: true,
        components: {
          default: () => import(
            /* webpackChunkName: "routes" */
            `@/components/menu/organizar/List.vue`
          ),
        }
      }
    ]
  },
  {
    path: '/instalation/bis/data-api',
    meta: {},
    name: 'instalation/bis/data-api',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/instalation/bis/DataApi.vue`
    ),
  },
  {
    path: '/instalation/bis/waitProducts',
    meta: {},
    name: 'instalation/bis/waitProducts',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/instalation/bis/WaitProducts.vue`
    ),
  },
  {
    path: '/writePassword/:recoverCode',
    meta: {
      public: true
    },
    params: true,
    name: 'WritePasswordMail',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/WritePasswordMail.vue`
    )
  },
  {
    path: '/forgotPassword',
    meta: {
      public: true
    },
    name: 'ForgotPasswordMail',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/ForgotPasswordMail.vue`
    )
  },
  {
    path: '/login',
    meta: {
      public: true,
    },
    name: 'Login',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/Login.vue`
    )
  },
  {
    path: '/',
    meta: { },
    name: 'Root',
    redirect: {
      name: 'Login'
    }
  },
  {
    path: '/dashboard',
    meta: { breadcrumb: true },
    name: 'Dashboard',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/Dashboard.vue`
    )
  }
];
