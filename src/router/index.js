import Vue from 'vue';
import Router from 'vue-router';
import paths from './paths';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import InstalationMiddleware from './middleware/instalationMiddleware'

Vue.use(Router);

const router =  new Router({
  base: '/',
  mode: 'hash',
  linkActiveClass: 'active',
  routes: paths
});
// router gards
router.beforeEach((to, from, next) =>
{
  let token = router.app.$store.getters['userProfile/store/token']

  if(!to.meta.public && !token)
    router.push({ name: 'Login' })
  //else to.name != 'AccessDenied'
  else
  {
    //Middleware to instalation steps
    let middleware = new InstalationMiddleware(router, to, from)
    let blocked = middleware.run()

    if(!blocked)
    {
      NProgress.start();
      next();
    }
    else
    {
      let pushPath = middleware.pushTo()
      router.push(pushPath)
    }
      
  }
});

router.afterEach((to, from) => {
  NProgress.done();
});

export default router;
