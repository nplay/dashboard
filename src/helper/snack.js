
class SnackHelper
{
    constructor()
    {
        this.snackCfg = this.initialize()
    }

    initialize()
    {
        return {
            show: true,
            y: 'top',
            x: null, 
            color: 'green',
            text: 'Sucesso',
            timeout: 4000
        };
    }

    setSuccess(message)
    {
        this.snackCfg.color = 'green'
        this._setMessage(message)
        return this
    }

    _setMessage(message)
    {
        this.snackCfg.text = message
        return this
    }

    setError(message)
    {
        this.snackCfg.color = 'red'
        this._setMessage(message)
        return this
    }

    setWarning(message)
    {
        this.snackCfg.color = 'orange'
        this._setMessage(message)
        return this
    }

    display()
    {
        window.getApp.snackbar = Object.assign({}, this.snackCfg)
    }
}

export default SnackHelper