
import ObserverModel from './observerModel'

class ObserverCollectionModel
{
   constructor()
   {
      this.list = new Array();
   }

   set(name, method)
   {
      let observerModel = this.find(name)

      if(!name)
         throw new Error(`Observer ${name} not exists please add first`)
      
      observerModel.event = method
      return observerModel
   }

   add(name, method)
   {
      //Overwrite current method if already exists
      if(this.find(name))
         return this.set(name, method)

      return this.list.push(new ObserverModel(name, method))
   }

   exists(name)
   {
      return this.find(name) != null
   }

   find(name)
   {
      return this.list.find(observerModel => {
         return observerModel.name == name
      })
   }
}

export default ObserverCollectionModel