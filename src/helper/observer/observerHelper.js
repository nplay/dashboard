import ObserverCollectionModel from "./Model/observerCollection";

class ObserverHelper
{
   static _listenerCollection;

   static get list()
   {
      if(ObserverHelper._listenerCollection == null)
         ObserverHelper._listenerCollection = new ObserverCollectionModel()

      return ObserverHelper._listenerCollection
   }

   static async dispatch(name, ...params)
   {
      let observerModel = ObserverHelper.list.find(name)

      if(!name)
         throw new Error(`Observer ${name} not exists`)

      let parameters = params.map(p => {
         let scalarType = typeof p == 'string' || typeof p == 'number'
         return !scalarType ? JSON.stringify(p) :  `'${p}'`
      }).join(', ')

      let evaluateStr = `observerModel.event(${parameters})`;
      return eval(evaluateStr)
   }
}

export default ObserverHelper