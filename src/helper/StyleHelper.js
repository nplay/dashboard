

class StyleHelper
{
    constructor(html)
    {
        this._html = html
        this._doc = null
    }

    get doc()
    {
        if(!this._doc)
            this._doc = new DOMParser().parseFromString(this._html, 'text/html').body
        
        return this._doc
    }

    get innerHTML()
    {
        return this.doc.innerHTML
    }

    setRule(selectorText, cssText)
    {
        this.deleteRule(selectorText)
        let style = this.addRule(selectorText, cssText)
        this.renderStyle(style)
    }

    renderStyles()
    {
        let styles = this.doc.querySelectorAll('style')

        for(let _style of styles)
            this.renderStyle(_style)
    }

    renderStyle(style)
    {
        let rawStyle = new Array()

        for(let rule of style.sheet.cssRules)
            rawStyle.push(rule.cssText)

        style.innerHTML = rawStyle.join('\n')
    }

    addRule(selectorText, cssText)
    {
        let style = this.doc.querySelector('style')

        if(!style)
        {
            style = document.createElement('style')
            this.doc.appendChild(style)
        }

        style.sheet.insertRule(`${selectorText}${cssText}`)

        return style
    }

    deleteRule(selectorText)
    {
        let styles = this.doc.querySelectorAll('style')

        for(let _style of styles)
        {
            let idxRule = null
            for(let idx in _style.sheet.cssRules)
            {
                if(_style.sheet.cssRules.item(idx).selectorText != selectorText)
                    continue;

                idxRule = idx;
                break;
            }

            if(!idxRule)
                continue;

            _style.sheet.deleteRule(idxRule)
            break;
        }
    }
}

export default StyleHelper