
import SimpleDialog from "@/components/dialog/SimpleDialog.vue"

class SimpleDialogHelper
{
    constructor(id)
    {
        this.store = window.getApp.$store
        this.currentModel = null;
        this.ID = id
    }

    defaultModel()
    {
        return {
            id: this.ID,
            title: null,
            enabled: false,
            width: '70%',
            fullscreen: false,
            right: false,
            showToolbar: true,
            loaded: false,
            body: null
        }
    }

    width(val)
    {
        let dialog = this.get()
        dialog.width = val
        return this
    }

    get()
    {
        let dialog = this.store.getters['dialogs/get']({workflowID: this.ID})

        if(!dialog)
        {
            this.save(this.defaultModel())
            dialog = this.store.getters['dialogs/get']({workflowID: this.ID})
        }
        
        return dialog
    }

    setTitle(val)
    {
        let dialog = this.get()
        dialog.title = val
        return this
    }

    toggle()
    {
        let dialog = this.get(this.ID)
        dialog.enabled = !dialog.enabled
        return this
    }

    open()
    {
        let dialog = this.get(this.ID)
        dialog.enabled = true
        return this
    }

    close()
    {
        let dialog = this.get(this.ID)
        dialog.enabled = false
        return this
    }

    fullscreen(flag)
    {
        let dialog = this.get(this.ID)
        dialog.fullscreen = flag
        return this
    }

    showToolbar(flag)
    {
        let dialog = this.get(this.ID)
        dialog.showToolbar = flag
        return this
    }

    right(flag)
    {
        let dialog = this.get(this.ID)
        dialog.right = flag
        return this
    }

    loading(flag)
    {
        let dialog = this.get(this.ID)
        dialog.loaded = !flag
        return this
    }

    body(data)
    {
        let dialog = this.get(this.ID)
        dialog.body = data
        return this
    }

    save(payload)
    {
        this.store.dispatch('dialogs/set', {
            workflowID: this.ID,
            payload: payload
        })

        return this
    }
}

export {
    SimpleDialogHelper,
    SimpleDialog
}