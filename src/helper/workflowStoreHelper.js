
class WorkflowStoreHelper
{
    constructor(id, initialData, store)
    {
      this.store = store || window.getApp.$store

      this.ID = id
      initialData = initialData || new Object()

      let currentWorkflow = this.get()
      
      if(currentWorkflow == undefined)
      {
        initialData.id = this.ID
        this._commit('workflows', initialData)
      }
    }

    reset()
    {
      this.store.commit('resetWorkflow', this.ID)
    }

    get(id)
    {
      id = id || this.ID

      let exists = this.store.state.workflows.hasOwnProperty(id)

      if(!exists)
        this.reset();

      return this.store.state.workflows[id]
    }

    save(data)
    {
      this._commit('workflows', data)
      return this;
    }

    _commit(key, element)
    {
      this.store.commit(key, {property: this.ID, value: element})
    }
}

export default WorkflowStoreHelper