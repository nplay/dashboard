import Vue from 'vue'

export default {
        namespaced: true,

        state: {
            workflows: {}
        },

        getters: {
            get: (state) => ({workflowID}) => {
                return state.workflows[workflowID]
            }
        },

        mutations: {
            set(state, {workflowID, payload}){
                Vue.set(state.workflows, workflowID, payload)
            }
        },

        actions: {
            set({commit, state}, {workflowID, payload}) {
                commit('set', {workflowID, payload})
            },

            setRules({commit, state}, {workflowID, payload}) {
                commit('set', {workflowID, payload})
            },
        },

        modules: {
        }
}