
export default {
        namespaced: true,

        state: {
            popup: {}
        },

        getters: {
            get: (state) => {
                return state.popup
            }
        },

        mutations: {
            set(state, payload){
                state.popup = payload
            }
        },

        actions: {
            set({commit, state}, payload) {
                commit('set', payload)
            },
        },
}