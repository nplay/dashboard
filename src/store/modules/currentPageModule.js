
import Vue from 'vue'

let model = {
    loading: false,
    additionalData: {
        menuId: null,
        description: null
    }
}

export default {
        namespaced: true,

        state: {
            workflows: {}
        },

        getters: {
            get: (state) => ({workflowID}) => {

                let data = state.workflows[workflowID]

                if(!data)
                {
                    Vue.set(state.workflows, workflowID, model);
                    data = state.workflows[workflowID]
                }

                return data
            }
        },

        mutations: {
            set(state, {workflowID, payload}){
                //state.workflows[workflowID] = payload
                Vue.set(state.workflows, workflowID, payload);
            }
        },

        actions: {
            set({commit, state}, {workflowID, payload}) {
                commit('set', {workflowID, payload})
            },

            setLoading({commit, state, getters}, {workflowID, payload}) {

                let data = getters.get({workflowID: workflowID})
                data.loading = payload

                commit('set', {workflowID: workflowID, payload: data})
            },

            setDescription({commit, state, getters}, {workflowID, payload}) {

                let data = getters.get({workflowID: workflowID})
                data.additionalData.description = payload

                commit('set', {workflowID: workflowID, payload: data})
            },

            setMenuId({commit, state, getters}, {workflowID, payload}) {

                let data = getters.get({workflowID: workflowID})
                data.additionalData.menuId = payload

                commit('set', {workflowID: workflowID, payload: data})
            }
        },
}