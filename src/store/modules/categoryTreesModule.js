import IndexSearchesAPI from "@/api/searches/indexApi.js";
import Vue from 'vue'

export default {
        namespaced: true,

        state: {
            workflows: {}
        },

        getters: {
            get: (state) => ({workflowID}) => {
                return state.workflows[workflowID]
            }
        },

        mutations: {
            set(state, {workflowID, payload}){
                Vue.set(state.workflows, workflowID, payload)
            }
        },

        actions: {
            set({commit, state}, {workflowID, payload}) {
                commit('set', {workflowID, payload})
            },

            async load({commit, getters}, {workflowID}) {

                let data = getters.get({workflowID: workflowID})
                
                if(!data || data.length == 0)
                {
                    let searchAPI = new IndexSearchesAPI();
                    let response = await searchAPI.all();
    
                    commit('set', {
                        workflowID: workflowID,
                        payload: response.data.data
                    })
                }

                return true
            },
        },

        modules: {

            activeNode: {
                namespaced: true,

                state: {
                    workflows: {}
                },

                getters: {
                    get: (state) => ({workflowID}) => {
                        return state.workflows[workflowID]
                    }, // -> getters['categoryTrees/activeNode/get']
                },

                mutations: {
                    set(state, {workflowID, payload}){
                        Vue.set(state.workflows, workflowID, payload)
                    }
                },

                actions: {
                    set({commit, state}, {workflowID, payload}) {
                        commit('set', {workflowID, payload})
                    },
                }
            },
        }
}