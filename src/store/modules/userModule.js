export default {
        namespaced: true,

        // module assets
        state: {
            profile: null
        },

        getters: {
            get: state => {
                return state.profile
            }
        },

        mutations: {
            set(state, payload){
                state.profile = payload
            }
        },

        actions: {
            set({commit, state}, payload) {
                commit('set', payload)
            }
        },

        // módulos aninhados
        modules: {

            //Current Store in dashboard
            store: {
                namespaced: true,

                state: {
                    store: null
                },

                getters: {
                    get: state => {
                        return state.store
                    }, // -> getters['userProfile/store/get']

                    token: state => {
                        let applications = state.store ? state.store.applications : new Array

                        let application = applications.find( application => {
                            return application.name == 'dashboard'
                        })

                        if(!application)
                            return null

                        let key = application.keys.find(key => {
                            return key.alias == 'default'
                        })

                        return !key ? null : key.value
                    }
                },

                mutations: {
                    set(state, payload) {
                        state.store = payload
                    }, // ['userProfile/store/set']
                },

                actions: {
                    set({ commit, state }, payload) {
                        commit('set', payload)
                    }
                }
            },

            stores: {
                namespaced: true,

                state: {
                    stores: new Array
                },

                getters: {
                    get: state => {
                        return state.stores
                    } // -> getters['userProfile/stores/get']
                },

                mutations: {
                    set(state, payload) {
                        state.stores = payload
                    } //['userProfile/stores/get']
                },

                actions: {
                    set({ commit, state }, payload) {
                        commit('set', payload)
                    }
                }
            },
        }
    }