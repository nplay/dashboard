
import Vue from 'vue'

let model = {
    xpath: null,
    enabled: true,
    name: null,
    themeLineCollection: new Array,
    additionalData: {
      currentOption: null
    },
    maxItens: 1,
    swiper: {
      slidesPerView: 1,
    },
    designModel: {
      html: null
    },
    currentLineModel: {
        swiper: {
          simulateTouch: true,
          slidesPerColumnFill: "column",
          slidesPerColumn: 1
        },
        isPreview: false,
        title: null,
        additionalData: {
            categoryID: null,
            custom: null
        },
        componentModel: {
            searchTypeId: 1,
            searchTypeDescription: null
        }
    }
  }

export default {
        namespaced: true,

        state: {
            workflows: {}
        },

        getters: {
            get: (state) => ({workflowID}) => {

                let data = state.workflows[workflowID]

                if(!data)
                {
                    //Vue.set(state.workflows, workflowID, model, Object.assign({}, model))
                    Vue.set(state.workflows, workflowID, model);
                    data = state.workflows[workflowID]
                }

                return data
            },
        },

        mutations: {
            set(state, {workflowID, payload}){
                //state.workflows[workflowID] = payload
                Vue.set(state.workflows, workflowID, payload);
            }
        },

        actions: {
            set({commit, state}, {workflowID, payload}) {
                commit('set', {workflowID, payload})
            },
        },

        modules: {
            line: {
                namespaced: true,
    
                state: {
                },
    
                getters: {
                    byID: (state, getters, rootState, rootGetter) => ({workflowID, lineID}) => {

                        let theme = rootGetter['theme/get']({workflowID: workflowID})
    
                        return theme.themeLineCollection.find(line => {
                            return line._id == lineID
                        })
                    },
                },
    
                mutations: {
                    
                },
    
                actions: {
                    set({ dispatch, commit, getters, rootGetters }, {workflowID, payload}) {

                        let theme = rootGetters['theme/get']({workflowID: workflowID})

                        let idx = theme.themeLineCollection.findIndex(line => {
                            return line._id == payload._id
                        })

                        Vue.set(theme.themeLineCollection, idx, payload)
                    },
                }
            }
        }
}