import Vue from 'vue'

let model = {
    fromDays: -7
}

export default {
        namespaced: true,

        state: {
            workflows: {}
        },

        getters: {
            get: (state) => ({workflowID}) => {
                
                let data = state.workflows[workflowID]

                if(!data)
                {
                    Vue.set(state.workflows, workflowID, Object.assign({}, model))
                    data = state.workflows[workflowID]
                }

                return data
            }
        },

        mutations: {
            set(state, {workflowID, payload}){
                Vue.set(state.workflows, workflowID, payload)
            }
        },

        actions: {
            set({commit, state}, {workflowID, payload}) {
                commit('set', {workflowID: workflowID, payload: payload})
            },
        },

        modules: {
        }
}