
import Request from "../request"

class IndexSearchesAPI
{
    constructor()
    {
        this._URL = 'searches/category/'
        this._request = new Request(this._URL)
    }

    all()
    {
        return this._request.get()
    }
}

export default IndexSearchesAPI 