
import Request from "./request"

class UserApi
{
    constructor()
    {
        this._URL = 'user/'
        this._request = new Request(this._URL, false, false)
    }

    login(userModel)
    {
        this._request = new Request('user/login', false, false)
        
        let data = {
            email: userModel.username,
            password: userModel.password
        }

        return this._request.post(data)
    }

    patch(data)
    {
        let tmpURL = `${this._URL}store/`
        this._request = new Request(tmpURL)
        let responseData = this._request.patch(data)
        this._request = new Request(this._URL)
        
        return responseData
    }
}

export default UserApi