
import Request from "../../../request"

class Create
{
    constructor()
    {
        this._URL = `user/contact`
        this._request = new Request(null)

        this._request
            .setURLROOT(this._request.store.state.env.API_SERVICE_HOST)
            .setURL(this._URL)
    }

    get(data)
    {
        return this._request.post(data)
    }
}

export default Create