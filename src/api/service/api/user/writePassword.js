
import Request from "../../../request"

class WritePassword
{
    constructor()
    {
        this._URL = `user/write-password`
        this._request = new Request(null)

        this._request
            .setURLROOT(this._request.store.state.env.API_SERVICE_HOST)
            .setURL(this._URL)
    }

    get(data)
    {
        return this._request.post(data)
    }
}

export default WritePassword