
import Request from "../../../request"

class CategoryRequest
{
    constructor()
    {
        this._URL = `category/request`
        this._request = new Request(null)

        this._request
            .setURLROOT(this._request.store.state.env.API_SERVICE_HOST)
            .setURL(this._URL)
    }

    get(data)
    {
        return this._request.put(data)
    }
}

export default CategoryRequest