
import Request from "../../../request"

class CategoryRequest
{
    constructor()
    {
        this._URL = `category/reorder`
        this._request = new Request(null)

        this._request
            .setURLROOT(this._request.store.state.env.API_SERVICE_HOST)
            .setURL(this._URL)
    }

    execute(menuID)
    {
        this._request.setURL(`${this._URL}/${menuID}`)
        return this._request.put({})
    }
}

export default CategoryRequest