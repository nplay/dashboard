
import Request from "../../request"

class StorageSimilarRequest
{
    constructor()
    {
        this._URL = `storage/similarRequest`
        this._request = new Request(null)

        this._request
            .setURLROOT(this._request.store.state.env.API_SERVICE_HOST)
            .setURL(this._URL)
    }

    get()
    {
        return this._request.put({})
    }
}

export default StorageSimilarRequest