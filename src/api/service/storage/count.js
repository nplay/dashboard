
import Request from "../../request"

class Count
{
    constructor()
    {
        this._URL = `storage/count`
        this._request = new Request(null)

        this._request
            .setURLROOT(this._request.store.state.env.API_SERVICE_HOST)
            .setURL(this._URL)
    }

    get()
    {
        return this._request.get()
    }
}

export default Count