
import Request from "../../request"

class BuyToBuyRequest
{
    constructor()
    {
        this._URL = `analytics/buyToBuyRequest`
        this._request = new Request(null)

        this._request
            .setURLROOT(this._request.store.state.env.API_SERVICE_HOST)
            .setURL(this._URL)
    }

    get()
    {
        return this._request.put({})
    }
}

export default BuyToBuyRequest