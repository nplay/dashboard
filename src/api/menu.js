let Menu = [
  {
    id: 'dashboard',
    title: 'Dashboard',
    name: 'Dashboard',
    icon: 'analytics',
    header: true
  },

  {
    id: 'personalizar',
    title: 'Personalizar',
    icon: 'format_paint',
    items: [
      {
        id: "personalizar.categorias",
        title: 'Categorias',
        name: 'menu/organizar'
      },
      {
        id: 'personalizar.vitrine',
        title: 'Temas de vitrine',
        icon: 'view_module',
        items: [
          {
            id: "personalizar.vitrine.home",
            title: 'Home',
            name: 'tema/list',
            params: {"typeTheme": "home"}
          },
          {
            id: 'personalizar.vitrine.category',
            title: 'Categoria',
            name: 'tema/list', 
            params: {"typeTheme": "category"}
          },
          {
            id: 'personalizar.vitrine.product',
            title: 'Produto', 
            name: 'tema/list', 
            params: {"typeTheme": "product"}
          },
          {
            id: 'personalizar.vitrine.cart',
            title: 'Carrinho', 
            name: 'tema/list', 
            params: {"typeTheme": "cart"}
          }
        ]
      },
      {
        id: 'personalizar.popups',
        title: 'Pop ups',
        icon: 'view_carousel',
        items: [
          {
            id: 'personalizar.popups.all',
            title: 'Todas', 
            name: 'tema/popup/list',
            params: {"type": "all"}
          },
          {
            id: 'personalizar.popups.cart',
            title: 'Carrinho', 
            name: 'tema/popup/list',
            params: {"type": "cart"}
          },
          {
            id: 'personalizar.popups.home',
            title: 'Home',
            name: 'tema/popup/list',
            params: {"type": "home"}
          }
        ]
      }
    ]
  }
];

let mapParent = (map = {}, nodes = Menu) => {

  for(let node of nodes)
  {
      map[node.id] = node

      if(node.items)
          mapParent(map, node.items)
  }

  return map
}

let Breadcrumb = (id) => {
  let map = mapParent()
  let keys = id.split('.')
  let fullKey = []

  let breadcumbs = []

  for(let key of keys)
  {
      fullKey.push(key)
      let node = map[fullKey.join('.')]

      let breadcumb = {
          id: node.id,
          text: node.title,
          disabled: node.hasOwnProperty('items'),
          node: node
      }

      breadcumbs.push(breadcumb)
  }

  return breadcumbs
}

export {
  Menu,
  Breadcrumb
}