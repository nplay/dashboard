
import ResponseTransform from './request/helper/responseTransform'

class Request
{
    constructor(url, notUseUrlRoot, useAuthentication)
    {
        this.vue = window.getApp
        this.axios = this.vue.axios
        this.store = this.vue.$store

        this.token = null
        this.useAuthentication = undefined == useAuthentication ? true : useAuthentication
        this.setURLROOT(this.store.state.env.API_HOST)
        this.setURL(url)
    }

    setURLROOT(data)
    {
        this.URL_ROOT = data
        return this
    }

    setURL(data, notUseUrlRoot = false)
    {
        this.url = notUseUrlRoot ? url : `${this.URL_ROOT}/${data}`
        return this
    }

    _getConfig()
    {
        this.token = this.useAuthentication ? this.store.getters['userProfile/store/token'] : null

        this.config = {
            headers: {'Authorization-token': this.token},
            responseType: 'json',
            validateStatus: function (status) {
                return status == 200 || status == 400; // default
            }
        }

        return this.config
    }

    post(body)
    {
        let response = this.axios.post(this.url, body, this._getConfig(null))
        return this.dispatch(response)
    }

    async dispatch(response)
    {
        try {
            var _response = await response
        } catch (error) {
            new ResponseTransform().run(error.response)
        }
        finally
        {
            return _response
        }
    }

    get(body)
    {
        let data = Object.assign(this._getConfig(), {data: body})
        let response =  this.axios.get(this.url, data);
        return this.dispatch(response)
    }

    delete(body)
    {
        let data = Object.assign(this._getConfig(), {data: body})
        let response =  this.axios.delete(this.url, data);
        return this.dispatch(response)
    }

    put(body)
    {
        let response =  this.axios.put(this.url, body, this._getConfig());
        return this.dispatch(response)
    }

    patch(body)
    {
        let response = this.axios.patch(this.url, body, this._getConfig());
        return this.dispatch(response)
    }
}

export default Request