
import SnackHelper from "@/helper/snack.js";

class ResponseTransform
{
    get RESOURCE_DISABLED_CODE()
    {
        return 1
    }

    run(response)
    {
        let code = response.data.code

        if(code == this.RESOURCE_DISABLED_CODE)
        {
            window.getApp.$router.replace({name: 'AccessDenied'})
            //window.getApp.$router.go(-1)
            new SnackHelper()
                .setWarning('Ops... seu plano não possui acesso a este recurso.')
                .display()
        }

        console.log(response, window.getApp)
    }
}

export default ResponseTransform