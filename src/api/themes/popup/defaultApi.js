
import Request from "../../request"

class DefaultApi
{
    constructor()
    {
        this._URL = 'themes/popup/default'
        this._request = new Request(this._URL)
    }

    all()
    {
        return this._request.get()
    }

    
}

export default DefaultApi