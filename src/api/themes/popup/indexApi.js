
import Request from "../../request"

class IndexAPI
{
    constructor()
    {
        this._URL = 'themes/popup/'
        this._request = new Request(this._URL)
    }

    all(body)
    {
        body = Object.assign({
            filters: {},
            attributes: {}
        }, body)
        
        return this._request.post(body)
    }

    put(data)
    {
        return this._request.put(data)
    }

    delete(id)
    {
        return this._request.delete(id)
    }

    patch(data, objectID)
    {
        let tmpURL = this._URL + objectID
        this._request = new Request(tmpURL)
        let responseData = this._request.patch(data)
        this._request = new Request(this._URL)
        
        return responseData
    }

    delete(objectID)
    {
        let tmpURL = this._URL + objectID
        this._request = new Request(tmpURL)
        let responseData = this._request.delete()
        this._request = new Request(this._URL)
        
        return responseData
    }
}

export default IndexAPI