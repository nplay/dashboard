
import Request from "../../request"

class DefaultApi
{
    constructor()
    {
        this._URL = 'themes/site/default'
        this._request = new Request(this._URL)
    }

    async byType(type = '')
    {
        let oldURL = this._URL

        try
        {
            this._URL = `${this._URL}/${type}`
            this._request = new Request(this._URL)

            return await this._request.get()
        }
        finally
        {
            this._URL = oldURL
            this._request = new Request(this._URL)
        }
    }

    all()
    {
        return this._request.get()
    }
}

export default DefaultApi