
import Request from "../request"

class AbstractCategoryModel
{
    constructor(data)
    {
        this.data = data
    }
}

class CustomizableCategoryModel extends AbstractCategoryModel
{
    constructor(data)
    {
        super(data)
    }

    get name()
    {
        return 'Personalizado'
    }
}

class Invoker
{
    get(id, data)
    {
        let instances = this.getAll()

        if(instances.hasOwnProperty(id))
        {
            let instance = instances[id]
            let object = new instance.__proto__.constructor(data)
            return object
        }

        throw new Error('Instance not found')
    }

    getAll()
    {
        return new Object({
            3: new CustomizableCategoryModel
        })
    }
}

class MenuApi
{
    constructor()
    {
        this._URL = 'themes/menu/'
        this._request = new Request(this._URL)
    }

    all()
    {
        return this._request.get()
    }

    delete(ID)
    {
        this._request = new Request(`${this._URL}${ID}`)
        return this._request.delete()
    }

    insert(menuModel)
    {
        return this._request.post(menuModel)
    }
}

export {
    MenuApi,
    Invoker,
    CustomizableCategoryModel
} 