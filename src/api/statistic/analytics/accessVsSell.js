
import Request from "../../request"

class AccessVsSell
{
    constructor()
    {
        this._URL = 'statistic/analytics/access-vs-sell'
        this._request = new Request(null)

        this._request
            .setURLROOT(this._request.store.state.env.API_SERVICE_HOST)
            .setURL(this._URL)
    }

    get(startDate, endDate)
    {
        let data = {
            "startDate": startDate,
            "endDate": endDate,
        }

        return this._request.post(data)
    }
}

export default AccessVsSell