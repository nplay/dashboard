
import Request from "../request"

class UpdateCategoryApi
{
    constructor()
    {
        this._URL = 'category/'
        this._request = new Request(this._URL)
    }

    update(menuID)
    {
        this._request = new Request(this._URL + menuID)

        return this._request.put({})
    }
}

export default UpdateCategoryApi