
import Request from "../request"

class SortAPI
{
    constructor()
    {
        this._URL = 'category/sort'
        this._request = new Request(this._URL)
    }

    async sort(level, by, menu)
    {
        let sortCollection = [
            {
                level: level,
                by: by,
                menu: menu
            }
        ]

        let requestData = {
            sorts: sortCollection,
            menu: menu
        }

        this._request = new Request(this._URL)
        let response = await this._request.post(requestData)
        return response.data.data
    }
}

export default SortAPI