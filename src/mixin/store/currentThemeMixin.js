
const Mixin = {

    computed: {
        
        currentTheme: {
            get: function(){
                return this.$store.getters['theme/get']({workflowID: 'currentTheme'})
            },

            set: function(val){
                this.$store.dispatch('theme/set', {
                    workflowID: 'currentTheme',
                    payload: val
                })
            }
        }
   }
}

export default Mixin