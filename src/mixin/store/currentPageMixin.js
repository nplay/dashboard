
const Mixin = {

    computed: {
        
        currentPage() {
            return this.$store.getters['currentPage/get']({workflowID: 'default'})
        },

        currentPageDescription: {
            get: function(){
                return this.currentPage.additionalData.description
            },

            set: function(val){
                this.$store.dispatch('currentPage/setDescription', {
                    workflowID: 'default',
                    payload: val
                })
            }
        },

        currentPageLoading: {
            get: function(){
                return this.$store.getters['currentPage/get']({workflowID: 'default'}).loading
            },

            set: function(val){
                this.$store.dispatch('currentPage/setLoading', {
                    workflowID: 'default',
                    payload: val
                })
            }
        }
   }
}

export default Mixin