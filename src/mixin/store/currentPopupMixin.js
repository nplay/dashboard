
const Mixin = {

    computed: {
        
        currentPopup: {
            get: function(){
                return this.$store.getters['popup/get']
            },
            set: function(val){
                this.$store.dispatch('popup/set', val)
            }
        }
   }
}

export default Mixin