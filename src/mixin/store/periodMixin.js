
const Mixin = {

    computed: {
        period: {
            get: function(){
                return this.$store.getters['period/get']({workflowID: this.periodWorkflowID})
            },

            set: function(val){
                this.$store.dispatch('period/set', {
                    workflowID: this.periodWorkflowID,
                    payload: data
                })
            }
        },

        periodWorkflowID: function(){
            return this.workflowID
        }
   },
}

export default Mixin