
const Mixin = {

    computed: {
        
        themeLineFilter: {
            get: function(){
                return this.$store.getters['themeLineFilter/get']({workflowID: this.themeLineFilterWorkflowID})
            },

            set: function(val){
                this.$store.dispatch('themeLineFilter/set', {
                    workflowID: this.themeLineFilterWorkflowID,
                    payload: val
                })
            }
        },

        themeLineFilterWorkflowID: function(){
            return this.workflowID
        }
   }
}

export default Mixin