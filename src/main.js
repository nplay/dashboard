import Vue from "vue";
import './plugins/vuetify'
import App from "./App.vue";
import store from "./store";
import router from "./router/";
import axios from 'axios'
import VueAxios from 'vue-axios'
import "./registerServiceWorker";
import "roboto-fontface/css/roboto/roboto-fontface.css"
import "font-awesome/css/font-awesome.css"

Vue.config.productionTip = false;

Vue.use(VueAxios, axios)

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");