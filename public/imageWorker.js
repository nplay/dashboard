
self.addEventListener('install', function(event) {
});

self.addEventListener('activate', function(event) {
});

self.addEventListener('fetch', (evt) => {

  let fnProcess = async () => {
    if(evt.request.destination != "image")
      return await fetch(evt.request)

    let response = await fetch(evt.request)

    let blob = await response.blob()
    let mimeType = blob.type

    let typeQualified = mimeType.match(/image\/.+$/) != null

    return typeQualified  == false ? await fetch('static/defaultProduct.png') : await fetch(evt.request)
  }

  evt.respondWith(
    fnProcess()
  );
})